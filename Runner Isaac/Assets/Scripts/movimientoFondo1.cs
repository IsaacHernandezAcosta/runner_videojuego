using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimientoFondo1 : MonoBehaviour
{
    public int vel;
    public GameObject posicio;
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<Rigidbody2D>().transform.position.x < -15)
        {
            this.GetComponent<Rigidbody2D>().transform.position = new Vector2(14.97f, -0.0054f);
        }
    }
}
