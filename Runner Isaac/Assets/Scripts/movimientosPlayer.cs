using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class movimientosPlayer : MonoBehaviour
{
    public GameObject bala;
    public int vel;
    public delegate void CogerDetonador();
    public event CogerDetonador OnCogerDetonador;
    public puntos puntosManager;
    public GameObject puntos;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 500));
        }
        if(this.GetComponent<Transform>().position.y< -8)
        {
            SceneManager.LoadScene("GameOver");
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            GameObject bala1 = Instantiate(bala);
            bala1.GetComponent<Transform>().position = this.gameObject.GetComponent<Transform>().position;
            bala1.GetComponent<Rigidbody2D>().velocity = Vector2.right * 4;
            bala1.GetComponent<balaMoviments>().onPunto += puntos.GetComponent<puntos>().ActivarPuntos;
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "detonador")
        {
          //  this.GetComponent<AudioSource>().Play();
            if (OnCogerDetonador != null)
            {
                OnCogerDetonador.Invoke();
            }
            Destroy(this.gameObject);
        }


        if (collision.transform.tag == "bomba")
        {
            Destroy(this.gameObject);
            SceneManager.LoadScene("GameOver");
        }
    }
}
