using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dollarMoviment : MonoBehaviour
{
    public int vel = 2;
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
        if (this.GetComponent<Transform>().position.x < -10)
        {
            Destroy(this.gameObject);
            vel++;
        }
    }
}
