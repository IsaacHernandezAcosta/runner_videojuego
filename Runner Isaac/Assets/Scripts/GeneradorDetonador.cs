using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static movimientosPlayer;
using UnityEngine.SceneManagement;

public class GeneradorDetonador : MonoBehaviour
{
    public GameObject detonador;
    void Start()
    {
        StartCoroutine(GenerarDetonador());
    }

    IEnumerator GenerarDetonador()
    {
        while (true)
        {
            print("Nuevo detonador creado");
            Instantiate(detonador);
            Vector2 pos = new Vector2(this.gameObject.transform.position.x, UnityEngine.Random.Range(2f, -4f));
            detonador.GetComponent<Transform>().position = this.gameObject.transform.position;
            detonador.GetComponent<Rigidbody2D>().velocity = Vector2.left * 5;
            yield return new WaitForSeconds(1.0f);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "putin")
        {
            Destroy(this.gameObject);
        }

    }
}
