using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimientoDetonador : MonoBehaviour
{
    public int vel = 2;
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, 0);
    }

    void Update()
    {
        if (this.GetComponent<Transform>().position.x < -14)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "bala")
        {
            Destroy(this.gameObject);
        }

    }
}
