using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimientoBomba : MonoBehaviour
{
    public int vel = 2;
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, 0);
    }


    void Update()
    {
        if (this.GetComponent<Transform>().position.y < -5)
        {
            Destroy(this.gameObject);
        }
    }
        
}
