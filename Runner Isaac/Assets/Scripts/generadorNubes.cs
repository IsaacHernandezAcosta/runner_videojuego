using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generadorNubes : MonoBehaviour
{
    public GameObject nube;
    void Start()
    {
        StartCoroutine(GenerarNubes());
    }


    IEnumerator GenerarNubes()
    {
        while (true)
        {
            print("Nubes");
            Instantiate(nube);
            Vector2 pos = new Vector2(this.gameObject.transform.position.x, UnityEngine.Random.Range(0, 7));
            nube.GetComponent<Transform>().position = this.gameObject.transform.position;
            nube.GetComponent<Rigidbody2D>().velocity = Vector2.left * 2;
            yield return new WaitForSeconds(2.0f);
        }
    }
}
