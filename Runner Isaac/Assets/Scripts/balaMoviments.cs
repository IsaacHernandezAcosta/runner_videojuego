using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class balaMoviments : MonoBehaviour
{
    public delegate void punto();
    public event punto onPunto;

    
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<Transform>().position.x > 14)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "detonador")
        {
            Destroy(collision.gameObject);

            if (onPunto != null)
                onPunto.Invoke();

            Destroy(this.gameObject);
        }
    }

}



