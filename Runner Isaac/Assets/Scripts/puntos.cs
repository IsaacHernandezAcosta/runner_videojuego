using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class puntos : MonoBehaviour
{
    int puntuacion;
    void Start()
    {
        puntuacion = 0;
        this.gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "Puntos: 0";
    }

    // Update is called once per frame
    void Update()
    {
    
    }

    public void ActivarPuntos()
    {
        puntuacion += 20;
        this.GetComponent<TextMeshProUGUI>().text = "Puntos: " + puntuacion;
    }
}
