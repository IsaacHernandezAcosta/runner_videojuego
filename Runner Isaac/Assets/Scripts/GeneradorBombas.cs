using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorBombas : MonoBehaviour
{

    public GameObject bombas;
    public movimientosPlayer putin;
    void Start()
    {
        
        putin.OnCogerDetonador += activar;
       
    }

 

    public void activar()
    {
        StartCoroutine(GenerarBombas());
        if (!this.GetComponent<BoxCollider2D>().isTrigger)
        {
            
            print("Putin ha muerto por una bomba...");
            Destroy(putin);
        }
        IEnumerator GenerarBombas()
        {
           
                print("Nueva bomba creada");
                Instantiate(bombas);
                Vector2 pos = new Vector2(this.gameObject.transform.position.x, UnityEngine.Random.Range(0, 7));
                bombas.GetComponent<Transform>().position = this.gameObject.transform.position;
                bombas.GetComponent<Rigidbody2D>().velocity = Vector2.left * 2;
                yield return new WaitForSeconds(4.0f);
            
        }
    }

    private void OnDisable()
    {
        putin.OnCogerDetonador -= activar;
    }

}
