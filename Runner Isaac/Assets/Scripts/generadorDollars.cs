using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generadorDollars : MonoBehaviour
{
    public GameObject dollars;
    void Start()
    {
        StartCoroutine(GenerarDollars());
    }


    IEnumerator GenerarDollars()
    {
        while (true)
        {
            print("dollars");
            Instantiate(dollars);
            Vector2 pos = new Vector2(this.gameObject.transform.position.x, UnityEngine.Random.Range(-4, -2));
            dollars.GetComponent<Transform>().position = this.gameObject.transform.position;
            dollars.GetComponent<Rigidbody2D>().velocity = Vector2.left * 5;
            
            yield return new WaitForSeconds(1.0f);
        }
    }
}
